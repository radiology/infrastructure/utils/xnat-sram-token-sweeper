# https://xnat.instant-connect.net/xapi/swagger-ui.html

from sys import stdout, exit

import os

import xnat
import requests
import logging

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

# XNAT Constants...

xnat_address = os.environ.get("XNAT_ADDRESS", "http://localhost")

xnat_username = os.environ.get("XNAT_USERNAME", "admin")
xnat_password = os.environ.get("XNAT_PASSWORD", "admin")

# SRAM Constants...
sram_prefix = os.environ.get("SRAM_PREFIX", "sram-")

sram_scim_url = os.environ.get("SRAM_SCIM_URL", 'http://localhost/api/v2')
sram_scim_key = os.environ.get("SRAM_SCIM_KEY", 'secret')

# Helper routine to perform the SCIM API request
# Return the Resources list if successful,
# Exit in case of errors
def SRAM_SCIM_Resources(resource):

    response = requests.get(
        f"{sram_scim_url}/{resource}",
        headers={
            'Content-Type': 'application/json',
            'Authorization': f"Bearer {sram_scim_key}"
        }
    )

    if response.status_code != 200:
        logging.error(f"SRAM Connection error: {response.status_code}, reason: {response.text}")
        exit()
    
    return response.json()['Resources']

# First collect list of members who are active/non-expired members to any CO
# connected to our service...
sram_members = []

for group in SRAM_SCIM_Resources("Groups"):
    for member in group['members']:
        if member['value'] not in sram_members:
            sram_members.append(member['value'])

logging.debug(f"SRAM MEMBERS: {sram_members}")

# Now iterate through all users, and only select active/non-expired members
sram_users = []
for user in SRAM_SCIM_Resources("Users"):

    # Only select active users..
    if user['active'] and user['id'] in sram_members:
        sram_users += [f"{sram_prefix}{user['userName']}"]
    else:
        logging.info(f"Found inactive SRAM user: {user['userName']}")

logging.debug(f"SRAM USERS: {sram_users}")

# Connect to XNAT and process users, remove alias tokens for inactive users...

xnat_session = xnat.connect(xnat_address, user=xnat_username, password=xnat_password)

for user in list(xnat_session.users):
    logging.info(f"Processing user: {user}...")

    if user == xnat_username:
        logging.info(f"Ignoring user: {user}, because this is your service account")
        continue

    if not user.startswith(sram_prefix):
        logging.info(f"Ignoring user: {user}, because username does not start with {sram_prefix}")
        continue

    if user in sram_users:
        logging.info(f"Ignoring user: {user}, because he/she is in active users")
        continue
        
    try:
        tokens = xnat_session.get(f"/data/services/tokens/show/user/{user}").json()
    except Exception as e:
        logging.error(f"Exception during processing user: {user}, error: {str(e)}")
        continue
        
    for token in tokens:
        alias = token["alias"]

        logging.info(f"[{user}] Removing alias: {alias}")
        xnat_session.get(f"/data/services/tokens/invalidate/{alias}")
    
    # E O F