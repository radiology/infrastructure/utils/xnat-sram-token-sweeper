FROM alpine:3.6

ENV PYTHONUNBUFFERED=1

RUN apk add --update --no-cache python3 ca-certificates && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

COPY requirements.txt /tmp

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR /app

COPY sweep.py .

COPY sweep-cron /etc/cron.d/sweep-cron
RUN crontab /etc/cron.d/sweep-cron

CMD ["crond", "-f", "-d", "8"]